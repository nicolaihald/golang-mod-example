package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var port = "8080"

func main() {
	log.Printf("Listening on port %s ...", port)
	log.Fatal(http.ListenAndServe(":"+port, router()))
}

func router() http.Handler {
	r := mux.NewRouter()
	r.Path("/greeting").Methods(http.MethodGet).HandlerFunc(getGreeting)
	r.PathPrefix("/").HandlerFunc(getCatchAll)

	return r
}

func getCatchAll(w http.ResponseWriter, r *http.Request) {
	log.Printf("Processing request: %s", r.URL.Path)
	fmt.Fprintf(w, "CATCH ALL caught %s", r.URL.Path)
}

func getGreeting(w http.ResponseWriter, req *http.Request) {
	log.Printf("Greeting: %s", req.URL.Path)
	_, _ = w.Write([]byte("Hello, world!"))
}
