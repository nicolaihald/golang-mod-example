### Adding an Existing Cluster 
In order to add an existing cluster, you'll need to provide the following details in GetLab:

- **API URL** (required)   
It's the URL that GitLab uses to access the Kubernetes API. Kubernetes
exposes several APIs, we want the "base" URL that is common to all of them
  ```bash
  kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}' | pbcopy
  ```
- C**A Certificate**   
Get the certificate by running this command:

  ```bash
  kubectl get secret $(kubectl get secret | grep default-token | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode | pbcopy
  ```
- Service Token:   
GitLab authenticates against Kubernetes using service tokens, which are
scoped to a particular namespace.
The token used should belong to a service account with
cluster-admin privileges. 

  1. To create this service account:
      ```bash
      cat > gitlab-admin-service-account.yaml << EOF
      apiVersion: v1
      kind: ServiceAccount
      metadata:
        name: gitlab-admin
        namespace: kube-system
      ---
      apiVersion: rbac.authorization.k8s.io/v1beta1
      kind: ClusterRoleBinding
      metadata:
        name: gitlab-admin
      roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: cluster-admin
      subjects:
      - kind: ServiceAccount
        name: gitlab-admin
        namespace: kube-system
      EOF
      ```
  
  2. Apply the service account and cluster role binding to your cluster:

      ```bash
      kubectl apply -f ./gitlab-admin-service-account.yaml
      ```
      > **NOTE**: For GKE clusters, you will need the
`container.clusterRoleBindings.create` permission to create a cluster
role binding. You can follow the Google Cloud documentation to grant access.

  3. Retrieve the token for the `gitlab-admin` service account:
      ```bash
      kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') -o jsonpath="{['data']['token']}" | base64 --decode | pbcopy
      ```
